# Analysis 2019 DESY

This repository contains the code and folder structure to easily run the reconstruction on the DESY testbeam data using SciFi Reconstruction.
To use it, clone the repository and create your own branch to work with.

## Setup instructions
On gitlab, create a branch for yourself (a good name could be `<your name>-<something>`), then clone the repository on your system:

```
git clone ssh://git@gitlab.cern.ch:7999/scifi-telescope/analysis-2019-desy.git
cd analysis-2019-desy
git checkout <your branch>
```

As you may see there are some empty folders where data will be written, but the raw data folder is missing.
You need to create it and it has to have the name `raw_data`. If the data is not already on your system, you can put it here, otherwise you are better off creating a link to it (if you are working on the server, for example):

```
ln -s /path/to/rawdata raw_data
```

At this point you need to make sure that the system knows where the SciFi Reconstruction executables are and this is done by:
```
source /path/to/scifi-rec/build/activate.sh
```

You can place this line in your `.bashrc` or create a `setup.sh` file here, so each time you open a new terminal you just need to type `source setup.sh`.

## Let's reconstruct
Some scripts to ease the running of the reconstruction software are located in `scripts`. They are supposed to be run from the main folder, as `./scripts/<script name>`, otherwise you will mess up the paths.



