#!/usr/bin/env python3

from reco import run_reconstruction
from utils import parse_runs, read_runlist
import argparse
import concurrent.futures

def main():
    parser = argparse.ArgumentParser('Run reconstruction for a single run')
    parser.add_argument('runs', type=str, help='Runs to be processed (must be of the form \'X\', \'X-Y\' or \'X,Y\' or any combination of these)')
    parser.add_argument('--jobs', type=int, default=1, help='Number of parallel jobs to run')
    args = parser.parse_args()

    requested_runs = parse_runs(args.runs)
    available_runs = read_runlist()

    runs = [r for r in requested_runs if r in available_runs]

    print('processing runs:', runs)

    failed_runs = []

    with concurrent.futures.ProcessPoolExecutor(max_workers=args.jobs) as exe:
        # jobs will be a dictionay containing the submitted jobs as keys and 
        # the run number as value (useful for the print, to know which
        # job failed)
        jobs = {}
        
        for run in runs:
            jobs[exe.submit(run_reconstruction, run, no_progress=True, quiet=True)] = run
        
        # wait for all jobs to finish, if one failed raise an exception and print which one failed
        for finished in concurrent.futures.as_completed(jobs):
            if finished.exception():
                print('****RUN {} FAILED ****'.format(jobs[finished]))
                #raise finished.exception()
                failed_runs.append(jobs[finished])
    
    print("failed runs:", failed_runs)


if __name__ == '__main__':
    main()