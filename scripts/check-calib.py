#!/usr/bin/env python3

#!/usr/bin/env python3

import toml
import matplotlib.pyplot as plt
from os import listdir as ls
import os.path as op
import re
import numpy as np
import argparse
from utils import parse_runs

def doesMatch(string):
    prog = re.compile(r'run_\d+')
    return re.match(prog, string) is not None

def main():
    parser = argparse.ArgumentParser('Check calibration files')
    parser.add_argument('sensor', type=int, help='Sensor number')
    parser.add_argument('--runs', '-r', type=str, default='0-2000', help='Runs to be processed (must be of the form \'X\', \'X-Y\' or \'X,Y\' or any combination of these)')
    args = parser.parse_args()

    folders = list(sorted(filter(doesMatch, ls('calibration_files'))))

    all_calibrations = []
    runs = parse_runs(args.runs)

    for foldername in folders:
        data = np.loadtxt(op.join('calibration_files', foldername, 'calib_{:02d}.txt'.format(args.sensor)))
        run = int(foldername[-4:])
        if not run in runs:
            continue
        print(run)
        channel = data[:, 0]
        calibration = data[:, 1]
        #print(channel)
        #print(calibration)
        all_calibrations.append(calibration)
        plt.plot(channel, calibration, lw=0.2, alpha=0.2)
    
    all_calibrations = np.array(all_calibrations)
    print(all_calibrations.shape)
    # all_calib_masked = np.ma.masked_where(all_calibrations < -250, all_calibrations)
    all_calib_masked = np.ma.masked_where(all_calibrations < -1000, all_calibrations)

    avg_cal = np.median(all_calib_masked, axis=0)
    std_cal = np.std(all_calib_masked, axis=0)
    plt.errorbar(channel, avg_cal, yerr=0, c='r')

    plt.xlabel('Channel')
    plt.ylabel('Correction (fine clock cycles)')
    plt.grid(alpha=0.3)
    ylim = np.array(plt.gca().get_ylim())
    ax2 = plt.gca().twinx()
    ax2.set_ylim(ylim * 0.05787037037)
    ax2.set_ylabel('Correction (ns)')

    plt.show()

if __name__ == '__main__':
    main()