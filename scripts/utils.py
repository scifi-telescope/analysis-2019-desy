import pandas as pd

def parse_runs(runs, bd='-', gd=','):
    """Returns an array from a string.

    Accepts strings of the form 'X', 'X-Y', 'X,Y' or any combination of these and returs an array of integers.
    The form 'X-Y' returs all numbers between X and Y (X and Y included). X must be not greater than Y otherwise an empty array is returned
    The form 'X,Y' returs X and Y only.
    For example, 1-4,6,8-10 returns [1, 2, 3, 4, 6, 8, 9, 10]

    Keyword arguments:
    runs -- the string of runs
    bd -- character used to indicate blocks of numbers, '-' by default
    gd -- character to separate groups, ',' by default
    """
    run_array = []
    blocks = runs.split(gd)
    for block in blocks:
        try:
            run_array.append(int(block))
        except ValueError:
            block_limits = block.split(bd)
            if len(block_limits) != 2:
                raise ValueError("Invalid syntax: " + block)
            run_array.extend(range(int(block_limits[0]), int(block_limits[1])+1))
    
    return run_array


def read_runlist(filename='runlist.csv'):
    df = pd.read_csv(filename)
    runs = df['run'].tolist()
    return runs

