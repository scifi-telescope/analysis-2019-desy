#!/usr/bin/env python3

import uproot
import argparse
import awkward
import matplotlib.pyplot as plt
import numpy as np
from scipy import stats

PITCH = 0.250
N_CH = 512

def main():
    parser = argparse.ArgumentParser('Run analysis for a single run')
    parser.add_argument('run', type=int, help='Run number to be processed')
    parser.add_argument('--verbose', '-v', action='store_true')
    args = parser.parse_args()

    run_analysis(args.run)


def run_analysis(run):
    match_file = uproot.open('reconstructed_data/run_{:04d}_time-trees.root'.format(run))
    print(match_file.classes())
    branches = awkward.Table(match_file['tel7']['tracks_clusters_matched'].arrays(namedecode='utf-8'))
    print('available branches:', branches.columns)
    
    event_filter_ref = (branches['evt_ntracks'] == 1)
    ref = branches[event_filter_ref]
    # plt.hist(ref.trk_chi2/ref.trk_dof, bins=100)
    # plt.show()
    residuals = ref['trk_u'] - ref['clu_u']
    event_filter_mat = (ref['clu_size'] > 0) & (np.abs(residuals) < 1.0)
    mat = ref[event_filter_mat]
    print('n entries:                   ', len(branches))
    print('n entries w/ 1 track:        ', len(ref))
    print('n entries w/ 1 matched track:', len(mat))

    analyze_clusters(ref, mat)
    analyze_efficiency(ref, mat)
    analyze_time_propagation(ref, mat)
    analyze_ctr(ref, mat)


def analyze_ctr(ref, mat):
    t_delta = mat.trk_time - mat.clu_time
    print(np.std(t_delta))
    print(np.std(t_delta[(-30 < t_delta) & (t_delta < -26)]))
    print(np.std(t_delta[(-2 < t_delta) & (t_delta < 2)]))
    plt.hist(t_delta, bins=100, range=(-50, 0))
    #plt.hist2d(mat.trk_v, mat.clu_time, bins=(100, 100), range=((-30, 30), (-230, -190)))
    #plt.colorbar()

    plt.show()


def analyze_clusters(ref, mat):
    plt.hist(mat.clu_size)
    plt.show()


def analyze_residuals(ref, mat):
    pass


def analyze_time_propagation(ref, mat):
    plt.hist2d(mat.clu_time, mat.trk_v, bins=100)
    plt.xlabel('Cluster time')
    plt.ylabel('Track v')
    plt.colorbar()
    plt.show()


def analyze_efficiency(ref, mat):
    h_ref, edges = np.histogram(ref['trk_channel'], bins=512, range=[0, 512])
    h_mat, _ = np.histogram(mat['trk_channel'], bins=512, range=[0, 512])

    h_eff = h_mat/h_ref
    #plt.step(get_bin_centres(edges), h_eff, where='mid')

    # error estimation with binomial
    h_err = np.sqrt(h_ref * h_eff * (1-h_eff))/h_ref
    # improved error estimation
    eff, lower, upper = compute_efficiency(h_ref, h_mat)

    plt.errorbar(edges[:-1], eff, yerr=[eff-lower, upper-eff], ls='', marker='.')
    plt.errorbar(edges[:-1], h_eff, yerr=h_err, ls='', marker='.', c='k')

    plt.show()

def compute_efficiency(total, success, interval=0.95):
	"""
	Compute efficiency and lower, upper uncertainty bound.
	"""
	eff = success / total
	# Clopper–Pearson confidence interval; for details see:
	# https://en.wikipedia.org/wiki/Binomial_proportion_confidence_interval
	q0 = (1.0 - interval) / 2.0
	q1 = q0 + interval
	lower = stats.beta.ppf(q0, success, total - success + 1)
	upper = stats.beta.ppf(q1, success + 1, total - success)
	return eff, lower, upper


def get_bin_centres(bin_edges):
    return (bin_edges[:-1] + bin_edges[1:]) / 2


if __name__ == '__main__':
    main()
