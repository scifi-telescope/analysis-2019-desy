#!/usr/bin/env python3

import toml
import matplotlib.pyplot as plt
from os import listdir as ls
import os.path as op
import re
import numpy as np


def doesMatch(string):
    prog = re.compile(r'run_\d+-geo.toml')
    return re.match(prog, string) is not None

def main():
    files = list(sorted(filter(doesMatch, ls('geometry_files'))))

    run_n = []
    offsets = [[],[],[],[],[],[],[],[],[],[]]

    for filename in files:
        with open(op.join('geometry_files', filename), 'r') as f:
            geo = toml.load(f)
            run_n.append(int(re.search(r'\d+', filename)[0]))
            for i in range(2,12):
                offsets[i-2].append(geo['sensors'][i]['offset'][(i+1)%2])
    
    #plt.hist(offset_2, bins=500, range=(-2000, 2000))
    for i in range(10):
        plt.plot(run_n, offsets[i], marker='.', ls=('-' if i%2==0 else '--'), label=str(i+2)+(' y' if i%2==0 else ' x'))
    plt.grid(alpha=0.3)
    plt.legend()
    plt.show()


if __name__ == '__main__':
    main()