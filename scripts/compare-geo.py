#!/usr/bin/env python3

import toml
import matplotlib.pyplot as plt
from os import listdir as ls
import os.path as op
import re
import numpy as np
import argparse

def main():
    parser = argparse.ArgumentParser('Compare two geometry files')
    parser.add_argument('file1', type=str, help='Path to the first file')
    parser.add_argument('file2', type=str, help='Path to the second file')
    args = parser.parse_args()

    geo1 = toml.load(args.file1)
    geo2 = toml.load(args.file2)

    print('==================== BEAM ====================')
    beam1 = geo1['beam']
    beam2 = geo2['beam']
    print('delta slope x: {:+.5f} ({:+.5f} - {:+.5f}) degree'.format(
        np.rad2deg(beam1['slope'][0] - beam2['slope'][0]), 
        np.rad2deg(beam1['slope'][0]),
        np.rad2deg(beam2['slope'][0])
        ))
    print('delta slope y: {:+.5f} ({:+.5f} - {:+.5f}) degree'.format(
        np.rad2deg(beam1['slope'][1] - beam2['slope'][1]),
        np.rad2deg(beam1['slope'][1]),
        np.rad2deg(beam2['slope'][1])
        ))
    print('delta slope t: {:+.5f} ({:+.5f} - {:+.5f}) ns/mm'.format(
        beam1['slope'][2] - beam2['slope'][2],
        beam1['slope'][2],
        beam2['slope'][2]
        ))
    print('delta divergence x: {:+.5f} ({:+.5f} - {:+.5f}) degree'.format(
        np.rad2deg(beam1['divergence'][0] - beam2['divergence'][0]),
        np.rad2deg(beam1['divergence'][0]),
        np.rad2deg(beam2['divergence'][0])
        ))
    print('delta divergence y: {:+.5f} ({:+.5f} - {:+.5f}) degree'.format(
        np.rad2deg(beam1['divergence'][1] - beam2['divergence'][1]),
        np.rad2deg(beam1['divergence'][1]),
        np.rad2deg(beam2['divergence'][1])
        ))
    print('delta divergence t: {:+.5f} ({:+.5f} - {:+.5f}) ns/mm'.format(
        beam1['divergence'][2] - beam2['divergence'][2],
        beam1['divergence'][2],
        beam2['divergence'][2]
        ))

    print('=================== SENSORS ===================')
    for sensor1, sensor2 in zip(geo1['sensors'], geo2['sensors']):
        print('Sensor', sensor1['id'])
        print('delta x: {:+.3f} ({:+.3f} - {:+.3f})'.format(
            sensor1['offset'][0] - sensor2['offset'][0],
            sensor1['offset'][0],
            sensor2['offset'][0]
            ))
        print('delta y: {:+.3f} ({:+.3f} - {:+.3f})'.format(
            sensor1['offset'][1] - sensor2['offset'][1],
            sensor1['offset'][1],
            sensor2['offset'][1]
            ))
        print('delta z: {:+.3f} ({:+.3f} - {:+.3f})'.format(
            sensor1['offset'][2] - sensor2['offset'][2],
            sensor1['offset'][2],
            sensor2['offset'][2]
            ))
        print('delta t: {:+.3f} ({:+.3f} - {:+.3f})'.format(
            sensor1['offset'][3] - sensor2['offset'][3],
            sensor1['offset'][3],
            sensor2['offset'][3]
            ))

if __name__ == '__main__':
    main()